# Sahamati Certification Framework

## Guidelines

* [General implementation guidelines](guidelines/general-guidelines.md)
* [Standard definitions of Session ID and FI Status states](guidelines/session-id-and-fi-status-states.md)

## Certification Scenarios

* [AA - Sahamati certification Test Scenarios](certification-scenarios/aa.md)
* [FIP - Sahamati certification Test Scenarios](certification-scenarios/fip.md)
* [FIU - Sahamati certification Test Scenarios](certification-scenarios/fiu.md)

